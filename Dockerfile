FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Copy package.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Bundle app source
COPY . . 

EXPOSE 8080
CMD [ "npm", "start" ]
