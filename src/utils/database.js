const { Pool, Client } = require('pg');

const { host, user, database, password } = require('../../config/config').database;

function getConnection() {
    return new Pool({
        host,
        user,
        password,
        database,
        port: 5432
    });
}

async function query(str) {
    const pool = getConnection();
    const res = await pool.query(str);
    await pool.end();
    return res;
}

module.exports = {
    query
};
