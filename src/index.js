const express = require('express');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const dataloader = require('dataloader');
const { buildSchema } = require('graphql');

const database = require('./utils/database');
const schema = require('./schema');

const PORT = 8080;

const app = express();

app.use(cors())

app.get('/events', fetchEvents);
app.use('/graphql', graphqlHTTP(req => ({
    schema,
    context: {},
    graphiql: true
})));

async function fetchEvents(req, res) {
    const data = await database.query('SELECT * FROM events');
    res.json({ events: data.rows });
}

app.listen(PORT, () => console.log(`Listening at Port ${PORT}`));
