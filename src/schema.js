const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
} = require('graphql');

//@todo: refactor and pass in handlers 
const database = require('./utils/database');

async function fetchEvents(date) {
    const data = await database.query(`SELECT * FROM events WHERE local_date like '${date}'`);
    return data.rows;
}

const EventType = new GraphQLObjectType({
    name: 'Event',
    description: '...',
    fields: () => ({
        id: {
            type: GraphQLString,
            resolve: row => row.id
        },
        name: {
            type: GraphQLString,
            resolve: row => row.name
        },
        description: {
            type: GraphQLString,
            resolve: row => row.description
        },
        url: {
            type: GraphQLString,
            resolve: row => row.url
        },
        address: {
            type: GraphQLString,
            resolve: row => row.address
        }
    })
});

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        description: '...',
        fields: () => ({
            events: {
                type: new GraphQLList(EventType),
                args: {
                    date: { type: GraphQLString }
                },
                resolve: (root, args, content) => fetchEvents(args.date)
             }
        })
    })
});
