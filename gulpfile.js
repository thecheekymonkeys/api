const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const gutil = require('gulp-util');
const babel = require('gulp-babel');

gulp.task('watch', () => 
    gulp.watch('src/**/*.js', () => gulp.start('build'))
);

gulp.task('build', () => 
    gulp.src('src/**/*.js')
    .pipe(plumber({ errorHandler: (err) => {
        notify.onError({
            title: "Gulp error in " + err.plugin,
            message:  err.toString()
        })(err)
        gutil.beep();
    }}))
    .pipe(babel())
    .pipe(gulp.dest('lib'))
);
        
